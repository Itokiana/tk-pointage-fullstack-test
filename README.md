# Generalites
Le but va être de créer un API REST Laravel gérant ce système de pointage ainsi que d'une interface mobile React native.
## Configuration requises
### Backend:

 - **Laravel 9**
 - **PHP 8**
 - **MySql**

### Frontend:
- **Node ~<=14.19.0**
- **Expo**
# Mise en place du projet
## Configuration du backend
### Configuration de la BDD
Veuillez modifier les lignes suivantes avec la configuration de votre BDD dans le fichier **.env**:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=ma_db
DB_USERNAME=root
DB_PASSWORD=
```
### Lancement de la migration
Lancer la migration avec la commande suivante:
```
cd tk-pointage-backend
php artisan migrate
```
### Enregistrement des donnees seed
Lancer le seed afin d'ajouter des donnees fake avec la commande suivante:
```
cd tk-pointage-backend
php artisan db:seed
```
### Demarrage du serveur backend
Pour pouvoir lancer le serveur backend veuillez lancer la commande suivante:
```
cd tk-pointage-backend
php artisan serve
```
### Liste des endpoints
- **GET /employees**: lister les employees
- **GET /employees/{date_created}**: lister les employees et filtrer par date
- **POST /employees**: un endpoint qui permet de créer un employée
- **GET /horodators**:  lister tous les pointages
- **GET /horodators/{employee_id}/employee**: lister tous les pointages d'un employee
- **POST /horodators/check-in**: creer un check in
- **POST /horodators/check-out**: creer un check out

## Configuration du frontend
### Installation des modules requises du projet
Lancer la commande suivante pour installer les modules du frontend:
```
cd tk-pointage-frontend
yarn
```
si vous etes npm:
```
cd tk-pointage-frontend
npm i
```

### Lancement du projet frontend
Lancer le frontend avec la commande suivante:
```
cd tk-pointage-frontend
yarn start
```
si vous etes npm:
```
cd tk-pointage-frontend
npm run start
```
Puis scanner le QRcode avec l'application expo
