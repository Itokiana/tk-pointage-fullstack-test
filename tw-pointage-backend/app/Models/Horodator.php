<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horodator extends Model
{
    use HasFactory;

    protected $fillable = ['check_in', 'check_out', 'comment', 'employee_id'];

    public function employee()
    {
        return $this->belongsTo(Horodator::class);
    }
}
