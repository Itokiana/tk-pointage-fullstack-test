<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'firstName', 'department', 'dateCreated'];

    /**
     * Get the horodators for the employee.
     */
    public function horodators()
    {
        return $this->hasMany(Horodator::class);
    }
}
