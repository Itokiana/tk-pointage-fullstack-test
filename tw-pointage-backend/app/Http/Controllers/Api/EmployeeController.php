<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * GET /employees
     * 
     * Permet récupérer la liste des employées.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Employee::all();
    }

    /**
     * GET /employees/{date_created}/date-created
     * 
     * Permet d'ajouter un filtre pour les récupérer date de création ( e.g: "2021-01-05" ).
     *
     * @return \Illuminate\Http\Response
     */
    public function by_date_created($date_created)
    {
        return Employee::where('dateCreated', $date_created)->get();
    }


    /**
     * POST /employees
     * 
     * Permet de créer un employée.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'firstName' => 'required',
            'dateCreated' => 'required',
            'department' => 'required'
        ]);

        return Employee::create($request->all());
    }
}
