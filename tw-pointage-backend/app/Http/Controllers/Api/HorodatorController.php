<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Horodator;
use App\Models\Employee;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HorodatorController extends Controller
{
    /**
     * GET /horodators
     * 
     * Liste de tous les pointages
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Horodator::all();
    }

    /**
     * GET /horodators/{employee_id}/employee
     * 
     * Liste de tous les pointages par employee
     *
     * @return \Illuminate\Http\Response
     */
    public function by_employee($employee_id)
    {
        return Horodator::where('employee_id', $employee_id)->get();
    }

    /**
     * POST /horodators/check-in
     * 
     * Quand un employée fait un check-in
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function check_in(Request $request)
    {
        return Horodator::create([
            'check_in' => now(),
            'employee_id' => $request->input('employeeId')
        ]);
    }

    /**
     * POST /horodators/check-out
     * 
     * Quand un employée fait un checkout, 
     * on devrait calculer le temps entre le check-in et checkout et le stocker en BDD.
     * 
     * Les données doivent être stockés en BDD ( libre à vous de définir la techno )
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function check_out(Request $request)
    {
        $horodator = Horodator::whereDate('check_in', date('Y-m-d'))
                ->where('employee_id', $request->input('employeeId'))
                ->orderBy('id', 'desc')
                ->first();

        if($horodator !== NULL && $horodator->check_out == NULL) {
            $dateCheckIn = Carbon::parse($horodator->check_in);
            $duration = $dateCheckIn->diffInSeconds(now());
            return Horodator::where('employee_id', $request->input('employeeId'))
                            ->update([
                                'check_out' => now(),
                                'comment' => $request->input('comment'),
                                'duration' => $duration
                            ]);
        }
        return response()->json([
            'message' => 'Vous avez deja fait un check-out'
        ]);
    }
}
