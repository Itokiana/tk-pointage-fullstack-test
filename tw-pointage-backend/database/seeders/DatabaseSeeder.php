<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Employee::create([
            'name' => 'Koto',
            'firstName' => 'Rakoto',
            'department' => 'Informatique',
            'dateCreated' => now()
        ]);

        \App\Models\Employee::create([
            'name' => 'Be',
            'firstName' => 'Rabe',
            'department' => 'Logistique',
            'dateCreated' => now()
        ]);
        \App\Models\Employee::create([
            'name' => 'Soa',
            'firstName' => 'Rasoa',
            'department' => 'Commercial',
            'dateCreated' => now()
        ]);

        \App\Models\Horodator::create([
            'check_in' => '2022-05-01 16:15:57',
            'check_out' => '2022-05-01 18:15:57',
            'duration' => 7200,
            'comment' => 'Pause cloppe',
            'employee_id' => 1,
        ]);

        \App\Models\Horodator::create([
            'check_in' => '2022-05-01 20:15:57',
            'check_out' => '2022-05-01 21:15:57',
            'duration' => 3600,
            'comment' => 'Pause cloppe',
            'employee_id' => 1,
        ]);

        \App\Models\Horodator::create([
            'check_in' => '2022-05-01 16:15:57',
            'check_out' => '2022-05-01 18:15:57',
            'duration' => 7200,
            'comment' => 'Pause toilette',
            'employee_id' => 2,
        ]);

        \App\Models\Horodator::create([
            'check_in' => '2022-05-01 16:15:57',
            'check_out' => '2022-05-01 17:15:57',
            'duration' => 3600,
            'comment' => 'Pause petit dejeuner',
            'employee_id' => 3,
        ]);
    }
}
