<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\Employee;

use App\Http\Controllers\Api\EmployeeController;
use App\Http\Controllers\Api\HorodatorController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*----------------- EMPLOYEE ROUTE START ---------------------------------------------------- */
Route::get('/employees', [EmployeeController::class, 'index' ]);
Route::get('/employees/{date_created}/date-created', [EmployeeController::class, 'by_date_created']);
Route::post('/employees', [EmployeeController::class, 'store']);
/*------------------------------------------------------------------------------------------- */

/*----------------- HORODATOR ROUTE START ---------------------------------------------------- */
Route::get('/horodators', [HorodatorController::class, 'index' ]);
Route::get('/horodators/{employee_id}/employee', [HorodatorController::class, 'by_employee' ]);
Route::post('/horodators/check-in', [HorodatorController::class, 'check_in' ]);
Route::post('/horodators/check-out', [HorodatorController::class, 'check_out' ]);
/*------------------------------------------------------------------------------------------- */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
