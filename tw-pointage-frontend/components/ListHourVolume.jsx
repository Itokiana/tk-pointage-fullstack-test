import React from 'react';
import { FlatList, StyleSheet, View } from 'react-native';

import { Card, Title, Paragraph, Button, Divider } from 'react-native-paper';
import HourVolumeProgress from './HoursVolumeProgress';

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  progressBarStyle: {
    marginTop: 10
  },
  paragraphStyle: {
    marginTop: 20
  }
});

const ListHourVolume = (props) => {
  return (
    <View style={styles.container}>
      <FlatList
        data={props.employees}
        renderItem={({item}) => (
          <Card>
            <Card.Content>
              <Title>{ item.name }</Title>
              <Paragraph>{ item.department }</Paragraph>
              <Divider />
              <HourVolumeProgress employeeId={item.id} />
            </Card.Content>
            <Card.Actions>
              <Button
                onPress={() => props.seeEmployee(item)}
              >Voir</Button>
            </Card.Actions>
          </Card>
        )}
      />
    </View>
  );
}

export default ListHourVolume;