import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';

import { List } from 'react-native-paper';

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

const ListEmployee = (props) => {
  const selectEmployee = (employee) => {
    props.selectEmployee(employee)
  }
  return (
    <View style={styles.container}>
      <FlatList
        data={props.employeesData}
        renderItem={({item}) => (
          <List.Item
            title={item.name}
            description={item.department}
            left={props => <List.Icon {...props} icon="account" />}
            onPress={e => selectEmployee(item)}
          />
        )}
      />
    </View>
  );
}

export default ListEmployee;