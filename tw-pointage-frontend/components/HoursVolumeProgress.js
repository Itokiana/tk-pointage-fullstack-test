import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';

import { Paragraph, ProgressBar, Colors } from 'react-native-paper';

import HorodatorService from '../services/HorodatorService';

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22,
   paddingStart: 15,
   paddingEnd: 15,
   marginTop: 20
  },
  item: {
    marginTop: 10
  }
});

const HourVolumeProgress = (props) => {
  const [progressHours, setProgressHours] = useState(0.0)

  useEffect(() => {
    getEmployeeHours(props.employeeId)
  }, [])

  const setHoursProgress = (d) => {
    setProgressHours((d.reduce((n, {duration}) => n + duration, 0)/3600)/40)
  }

  const getEmployeeHours = (employeeId) => {
    HorodatorService.allHorodatorsByEmployee(employeeId)
    .then(response => {
      setHoursProgress(response.data)
    })
  }

  return (
    <View style={styles.container}>
      <Paragraph>Volume horaire</Paragraph>
      <ProgressBar style={styles.item} progress={progressHours} color={Colors.red800} />
    </View>
  );
}

export default HourVolumeProgress;