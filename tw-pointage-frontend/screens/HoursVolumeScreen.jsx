import * as React from 'react';
import ListHourVolume from '../components/ListHourVolume';
import EmployeeService from '../services/EmployeeService';


const HoursVolumeScreen = ({ navigation, route }) => {
  const [employees, setEmployees] = React.useState([])

  React.useEffect(() => {
    EmployeeService.allEmployees()
    .then(response => {
      setEmployees(response.data)
    })
  },[])

  const seeEmployee = (employee) => {
    navigation.navigate('Profil', employee)
  }

  return (
    <>
      <ListHourVolume employees={employees} seeEmployee={seeEmployee} />
    </>
  );
};

export default HoursVolumeScreen;
