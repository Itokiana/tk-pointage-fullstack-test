import * as React from 'react';
import { Button } from 'react-native';

import ListEmployee from '../components/ListEmployee';

import EmployeeService from '../services/EmployeeService';

const HomeScreen = ({ navigation }) => {
  const [employeesData, setEmployeesData] = React.useState([]);

  React.useEffect(() => {
    EmployeeService.allEmployees()
    .then(response => {
      setEmployeesData(response.data)
    })
  }, [])

  const seeEmployee = (employee) => {
    navigation.navigate('Profil', employee)
  }
  return (
    <>
      <Button
        title="Volume horaires"
        onPress={() =>
          navigation.navigate('Volume horaire')
        }
      />
      <ListEmployee employeesData={employeesData} selectEmployee={seeEmployee} />
    </>
  );
};

export default HomeScreen