import * as React from 'react';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';

import HoursVolumeProgress from '../components/HoursVolumeProgress';


const EmployeeProfileScreen = ({ navigation, route }) => {
  return (
    <>
    <Card>
      <Card.Content>
        <Title>{ route.params.name }</Title>
        <Paragraph>{ route.params.department }</Paragraph>
      </Card.Content>
    </Card>
    <HoursVolumeProgress employeeId={route.params.id} />
    </>
  );
};

export default EmployeeProfileScreen;
