import * as React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './screens/HomeScreen';
import EmployeeProfileScreen from './screens/EmployeeProfileScreen';
import HoursVolumeScreen from './screens/HoursVolumeScreen';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: 'Bienvenue' }}
        />
        <Stack.Screen name="Profil" component={EmployeeProfileScreen} />
        <Stack.Screen name="Volume horaire" component={HoursVolumeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;