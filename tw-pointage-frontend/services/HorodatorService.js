import { axiosInstance } from "../config/AxiosConfig"

class HorodatorService {
  allHorodators()  {
    return axiosInstance.get('/api/horodators');
  }
  allHorodatorsByEmployee(employeeId)  {
    return axiosInstance.get(`/api/horodators/${employeeId}/employee`);
  }
  checkIn(data)  {
    return axiosInstance.post('/api/horodators/check-in', data);
  }
  checkOut(data)  {
    return axiosInstance.post('/api/horodators/check-out', data);
  }
}

export default new HorodatorService();