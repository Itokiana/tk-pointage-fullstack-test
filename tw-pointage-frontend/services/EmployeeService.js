import { axiosInstance } from "../config/AxiosConfig"

class EmployeeService {
  allEmployees()  {
    return axiosInstance.get('/api/employees');
  }
  allEmployeesByDate(date)  {
    return axiosInstance.get(`/api/employees/${date}/date-created`);
  }
  newEmployee(data)  {
    return axiosInstance.post('/api/employees', data);
  }
}

export default new EmployeeService();